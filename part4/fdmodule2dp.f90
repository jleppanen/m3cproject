! Juha Leppanen, 00820470
!
!module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test function applies these methods to a Gaussian function and 
!returns the error.
!
! Compile with f2py --f90flags='-fopenmp -o3' -lgomp -llapack -c fdmodule.f90 hw41.f90 -m hw4

module fdmodule2d
!$    use omp_lib
    use fdmodule
    implicit none
    integer :: n1,n2,numthreads
    real(kind=8) :: dx1,dx2
    save
contains
!-------------------
subroutine grad(f,df1,df2)
    implicit none
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2
    real(kind=8), dimension(size(f,2),size(f,1)) :: f_temp, df1_temp
    integer :: i

    ! compute df1/df2 by keeping the other variable fixed whilst running cfd4 through the other dimension of f
    
    ! main loop in x1, setting the parameters for fmodule and transposing for efficient index accessing
    N = n1
    dx = dx1
    f_temp = transpose(f)
    do i = 1, n2
       call fd2(f_temp(:,i),df1_temp(:,i))
    end do
    df1 = transpose(df1_temp)

    ! main loop in x2, resetting the parameters for fmodule
    N = n2
    dx = dx2
    do i = 1, n1
       call fd2(f(:,i),df2(:,i))
    end do

end subroutine grad
!---------------------------
subroutine test_grad(error,time)
    !tests accuracy and speed of grad, assumes n1,n2,dx1,dx2 have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2 !test function and results frorom grad
    integer(kind=8) :: t1,t2,clock_rate
    real(kind=8), intent(out) :: time,error(2) !error is two-element array

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1))


    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = exp(-100.d0*((x11-0.5d0)**2+(x22-0.5d0)**2))!sin(x11)*cos(x22) !test function

    call system_clock(t1)
    call grad(ftest,df1,df2)
    call system_clock(t2,clock_rate)

    time = dble(t2-t1)/dble(clock_rate)

    error(1) = sum(abs(df1+200.d0*(x11-0.5d0)*ftest))/(n1*n2)!cos(x11)*cos(x22)))/(n1*n2)
    error(2) = sum(abs(df2+200.d0*(x22-0.5d0)*ftest))/(n1*n2)!sin(x11)*sin(x22)))/(n1*n2)
end subroutine test_grad
!-------------------
subroutine omp_rhs(f,RHS)
  use advmodule
  implicit none
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)) :: df1,df2
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: RHS
    real(kind=8), dimension(size(f,2),size(f,1)) :: f_temp, df1_temp
    integer :: i
! Returns the RHS of the 2D advection equation by computing the partial derivative in one dimension in a parallel loop first, then computing both the partial derivative and the final output RHS in another parallel loop
    
    !$ call omp_set_num_threads(numthreads)

    ! main loop in x1, setting the parameters for fmodule and transposing for efficient index accessing
    N = n1
    dx = dx1

    f_temp = transpose(f)
    !$OMP parallel do
    do i = 1, n2
       call fd2(f_temp(:,i),df1_temp(:,i))
    end do
    !$OMP end parallel do
    df1 = transpose(df1_temp)

    ! main loop in x2, resetting the parameters for fmodule and computing the output variable RHS as well
    N = n2
    dx = dx2

    !$OMP parallel do
    do i = 1, n1
       call fd2(f(:,i),df2(:,i))
       RHS(:,i) = s_adv - c1_adv*df1(:,i) - c2_adv*df2(:,i)
    end do
    !$OMP end parallel do


end subroutine omp_rhs

!-------------------
subroutine grad_omp(f,df1,df2)
    implicit none
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2
    real(kind=8), dimension(size(f,2),size(f,1)) :: f_temp, df1_temp
    integer :: i1

    !compute df/dx1
    n = n1
    dx = dx1
!!$    !$OMP parallel do
!!$    do i1=1,n2
!!$        call fd2(f(i1,:),df1(i1,:))
!!$    end do
!!$    !OMP end parallel do

    f_temp = transpose(f)
    !$OMP parallel do
    do i1 = 1, n2
       call fd2(f_temp(:,i1),df1_temp(:,i1))
    end do
    !$OMP end parallel do
    df1 = transpose(df1_temp)

    n = n2
    dx = dx2


    !$OMP parallel do
    do i1=1,n1
        call fd2(f(:,i1),df2(:,i1))
    end do
    !OMP end parallel do


end subroutine grad_omp
!---------------------------
subroutine test_grad_omp(error,time)
    !tests accuracy and speed of grad_omp, assumes n1,n2,dx1,dx2, and numthreads have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2 !test function and results frorom grad
    integer(kind=8) :: t1,t2,clock_rate
    real(kind=8), intent(out) :: time,error(2) !error is two-element array

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1))

    !$ call omp_set_num_threads(numthreads)

    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = exp(-100.d0*((x11-0.5d0)**2+(x22-0.5d0)**2))!sin(x11)*cos(x22) !test function

    call system_clock(t1)
    call grad_omp(ftest,df1,df2)
    call system_clock(t2,clock_rate)

    time = dble(t2-t1)/dble(clock_rate)

      error(1) = sum(abs(df1+200.d0*(x11-0.5d0)*ftest))/(n1*n2)!cos(x11)*cos(x22)))/(n1*n2)
    error(2) = sum(abs(df2+200.d0*(x22-0.5d0)*ftest))/(n1*n2)!sin(x11)*sin(x22)))/(n1*n2)

end subroutine test_grad_omp
!---------------------------
end module fdmodule2d













