! Juha Leppanen, 00820470
! Used to set parameters for other modules
! Have erased test_adv as it is unnecessary for this part of the project.

module advmodule
    implicit none
    real(kind=8) :: S_adv,c_adv
    real(kind=8) :: c1_adv,c2_adv !for 2d advection eqn.
    save
end module advmodule
