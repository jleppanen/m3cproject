# Juha Leppanen, 00820470
# Project part 4
# Contains a function to solve the 2D advection equation and a function to test and illustrate the main points about the solution
import numpy as np
import matplotlib.pyplot as plt
import adv2d
#compile all with 'make f2py'
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp


def advection2d(nt,tf,nx,ny,dx,dy,c1=1.0,c2=1.0,S=0.0,display=False,numthreads=1,par=0):
    """solve 2D advection equation, df/dt + c1 df/dx + c2 df/dy = S
    for x=0,dx,...,(nx-1)*dx, y=0,dy,...,(ny-1)*dy and returns f(x,y,tf),error  which is the solution obtained using the Fortran
    routine ode2d_rk4
    -    f(x,y,t=0) = exp(-100*((x-x0)^2+(y-y0)^2))
    -    nt time steps are taken from 0 to tf
    -    The contours of solution and absolute error are plotted if display is true

Notes on figures: The solution (contours.png) and error (error_contours.png) behave as expected, with the error symmetrically concentrated where the gradient of the solution is greatest.
    """
    # Define grid and initial condition
    x = np.arange(nx)*dx # grid in x dimension
    y = np.arange(ny)*dy # grid in y dimension
    mesh = np.meshgrid(x,y) # mesh
    x0 = 0.5
    y0 = 0.5
    f0 = np.exp(-100.0*((mesh[0]-x0)**2+(mesh[1]-y0)**2)) # initial condition
    
    # Define all the necessary parameters for Fortran modules
    adv2d.advmodule.c1_adv = c1
    adv2d.advmodule.c2_adv = c2
    adv2d.advmodule.s_adv = S
    adv2d.fdmodule2d.dx1 = dx
    adv2d.fdmodule2d.n1 = nx
    adv2d.fdmodule2d.dx2 = dy
    adv2d.fdmodule2d.n2 = ny
    adv2d.fdmodule2d.numthreads = numthreads
    adv2d.ode2d.par = par
    adv2d.ode2d.numthreads = numthreads
    
   
    # Main function call
    f,time = adv2d.ode2d.rk4(0.0,f0,tf/nt,nt)
    # Compute analytic solution, np.mod needed as we are only interested in region [0,1]x[0,1]
    analytic =  np.exp(-100.0*(((np.mod(mesh[0]-c1*tf,1))-x0)**2+(((np.mod(mesh[1]-c2*tf,1))-y0)**2)))+S*tf


    # Plot contours of function and absolute error if display flag is true
    if (display==True):
        levels = np.linspace(S+0.1,S+tf,10) # Set contour levels
        plt.figure()
        plt.contour(x,y,f,levels)
        plt.title('Juha Leppanen, advection2d\nContours of solution at time=%s for mesh size nx=%s,ny=%s'%(tf,nx,ny))
        plt.xlabel('x')
        plt.ylabel('y')
        plt.figure()
        plt.contour(np.arange(nx)*dx,np.arange(ny)*dy,np.abs(f-analytic))
        plt.title('Juha Leppanen, advection2d\nContours of absolute error at time=%s for mesh size nx=%s,ny=%s'%(tf,nx,ny))
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    return f, time,analytic


def test_advection2d(tf=1.0,display=False,parallel=False,numthreads=2):
    """
Computes the solution to 2D advection equation using a Runge-Kutta method with a 2nd order numerical method for the derivatives. Plots the error as N increases and gives a linear fit estimate for the rate of convergence. We can see that RoC is approximately 2 as is expected from the 2nd order stencil. Also if the flag parallel is True, runs the same calculation in parallel and plot error against N as well as the speedup between the parallel and serial methods.

We note that accuracy of the two methods is identical (convergence.png), as the computations behind both are exactly the same. Also the speedup for values of N within our scope is modest and also quite volatile (speedup.png) between different runs. This is not that surprising as the routines grad and grad_omp run in similar time after having made the change from cfd4 to fd2. This can be tested by calling test_grad() and test_grad_omp() through the .so file. Both have been modified to differentiate f0 as in this problem.

INPUTS: 
    - tf = time for which the solution is computed (default 1.0)
    - display = flag to control whether contours of the solution are plotted (default False)
    - parallel = plots speedup of parallel code vs. serial is flag is true
    - numthreads = defines how many threads are used if par=1 (default 2)

OUTPUTS: Plot of accuracy for serial version, also plots for accuracy and speedup if parallel=True
"""

    # Set parameters
    L = 1.0
    c1 = 1.0
    c2 = 1.0
    S = 1.0
    nt = 5000 # Reduced nt to make loops run in decent time
    
#------------------ACCURACY ASSESSMENT---------------------

    # Initialise arrays for accuracy assessment
    n_vals = np.arange(50,300,50) # Defines a square mesh
    error_ser = np.zeros(np.size(n_vals))
    time_ser = np.zeros(np.size(n_vals))
    
    # Loop to record error of serial method for different values of N (square mesh used as otherwise computations would take too long or would be restricted to small values of N1,N2)
    for i in range(np.size(n_vals)):
        nx = n_vals[i]
        ny = n_vals[i]
        dx = L/float(nx)
        dy = L/float(ny)
        x = np.arange(nx)*dx
        y = np.arange(ny)*dy
        f_ser, time_ser[i] ,analytic  = advection2d(nt,tf,nx,ny,dx,dy,c1,c2,S,display,numthreads,0)
        error_ser[i] = np.mean(np.abs(f_ser-analytic))

    # Fit a line through the loglog plot to estimate serial rate of convergence
    slope = -np.polyfit(np.log(n_vals),np.log(error_ser),1)[0]

    # Plot error against N
    plt.figure()
    plt.loglog(n_vals,error_ser,'b',label='Serial, Rate of convergence %.3f'%slope)
    plt.legend(loc='best')
    plt.title('Juha Leppanen, test_advection2d\nConvergence of error of serial rk4 method for 2D advection equation.')
    plt.xlabel('N')
    plt.ylabel('Error')
    

#------------------PARALLEL COMPARISON---------------------

    # If flag is true, do an identical loop for parallelised version of the code, as well as compute the speedup between the methods as well
    if (parallel==True):
        error_par = np.zeros(np.size(n_vals))
        speedup = np.zeros(np.size(n_vals))
        for i in range(np.size(n_vals)):
            nx = n_vals[i]
            ny = n_vals[i]
            dx = L/float(nx)
            dy = L/float(ny)
            x = np.arange(nx)*dx
            y = np.arange(ny)*dy
            f_par,time_par,analytic  = advection2d(nt,tf,nx,ny,dx,dy,c1,c2,S,display,numthreads,1)
            error_par[i] = np.mean(np.abs(f_par-analytic))
            speedup[i] = time_ser[i]/time_par


        # Fit a line through the loglog plot to estimate parallel rate of convergence
        slope = -np.polyfit(np.log(n_vals),np.log(error_par),1)[0]
        
        # Plot error against N
        plt.loglog(n_vals,error_par,'r',label='Parallel, Rate of convergence %.3f'%slope)
        plt.legend(loc='best')
        plt.title('Juha Leppanen, test_advection2d\nConvergence of error of parallel rk4 method for 2D advection equation.')
        plt.xlabel('N')
        plt.ylabel('Error')

        plt.figure()
        plt.title('Juha Leppanen, test_par_adv2d\nSpeedup of parallel code vs serial.')
        plt.plot(n_vals,speedup,'b*')
        plt.xlabel('N')
        plt.ylabel('Speedup')


if __name__=='__main__':

    # Plot the contours of the solution at t=1.0
    L = 1.0
    nt = 16000
    tf = 1.0
    nx = 200
    ny = 200
    dx = L/float(nx)
    dy = L/float(ny)
    f,time,analytic = advection2d(nt,tf,nx,ny,dx,dy,S=1.0,display=True,numthreads=2,par=1)

    # Plot the accuracy of serial&parallel methods and speedup between them
    test_advection2d(tf,False,True,2)
    plt.show()

