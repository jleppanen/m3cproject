""" Juha Leppanen, 00820470
Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d.
Dependencies compile using 'make f2py'.
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack

Added assertions include checks for input variables types of M, fac and boolean flags. The range of fac is tested in the loop via assert_check by printing an error if none or all pixels in all channels are rejected. This way we could easily extend the assertions to set boundaries for what level of thresholding is acceptable.
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display,savefig):
    """set all elements of M to zero where grad_amp < fac*max(grad_amp)
INPUTS:
    - M = image file in vectorised RGB form
    - fac = thresholding factor
    - display = show thresholded image if True
    - savefig = save thresholded image in "p2.png2" if True
OUTPUTS:
    - M = thresholded image
    - df_max = maximum of the derivatives in each colour channel
    """

    assert (type(np.shape(M))==tuple), 'M does not represent an image'
    assert (type(display)==bool and type(savefig)==bool), 'display,savefig have to be Boolean variables'
    assert (type(fac)==float), 'fac has to be a float'
    # Initialise variables
    df_max = np.zeros(3)
    f2d.n1 = np.shape(M)[1]
    f2d.n2 = np.shape(M)[0]
    f2d.dx1 = 1
    f2d.dx2 = 1
    assert_check = 0
    # Loop through the three colours (ie. indices of the dimension 3 of M)
    for i in range(np.shape(M)[2]):
        M_filter = np.asarray(M[:,:,i]) # Initialise filtering array
        df1,df2,df_amp,df_max[i] = f2d.grad_omp(M[:,:,i]) # Call main function
        idx_unsuitable = df_amp < fac*df_max[i] # Check which entries do not pass the criterion
        assert_check = assert_check + np.sum(idx_unsuitable)
        M_filter[idx_unsuitable] = 0 # Set the failed entries to 0
    assert (assert_check<np.shape(M)[0]*np.shape(M)[1]*np.shape(M)[2]), 'Threshold too strict, try increasing value fac' 
    assert (np.sum(idx_unsuitable)>0), 'No filtering with current parameters, try decreasing value fac'
                   
    # Show image if "display" flag is true
    if (display==True):
        plt.figure()
        plt.imshow(M)
        plt.show

    # Save image if "savefig" flag is true
    if (savefig==True):
        plt.savefig("p2.png")

    return M,df_max

if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True)
    plt.show()
    
