Juha Leppanen, 00820470

This is an overview of files modified in the project.


---------------PART 1---------------

Makefile || 'make f2py' compiles everything needed for running the Python program

fdmodule.f90 || Developed HW4 file to include cfd_2d
fdmodule2d.f90 || Developed HW4 file to include grad2d, test_grad2d
p1_3.py

data.in || Input for test_program
test_program.f90 || Tests DGTSV with multiple RHS columns

---------------PART 2---------------

Makefile || 'make f2py' compiles everything needed for running the Python program

fdmodule.f90 || Same as part1
fdmodule2d.f90 || Same as part1
p2.py || Developed the image thresholding code

p2.png || Thresholded image

---------------PART 3---------------

Makefile || 'make f2py' compiles everything needed for running the Python program, 'make mpi'&'make testmpi' runs the MPI variant

data.in || Input parameters for test program in advmodule.f90
advmodule.f90 || Commented out test program after use for easier compilation
fdmoduleB.f90 || Completed function RHS
ode.f90 || Developed RHS, euler_omp, RHS_omp and made correction as introduced in ode_v2.f90
p3_v2.py || Developed Python code to solve the advection equation using subroutines in ode.f90, and also to optionally compile and run MPI variant in shell with same parameters for easy comparability

data_mpi.in || Input parameters for MPI program
ode_mpi.f90 || Developed MPI version of the advection code
fmpi.dat || Output of MPI program

---------------PART 4---------------

Makefile || 'make f2py' compiles everything needed for running the Python program

advmodule2d.f90 || No changes made from course repo
fdmoduleB.f90 || No changes made from course repo
fdmodule2dp.f90 || Added subroutine omp_rhs that computes RHS of advection eqn in parallel, changed grad to use fd2 not cfd4, changed test_grad and test_grad_omp to test the same Gaussian as in the advection equation
ode2d_v2.f90 || Developed RHS to be suitable for both serial and parallel use
p4_v2.py || Developed advection2d to solve the advection equation in serial or parallel depending on inputs. Developed test_advection2d to illustrate the convergence rate of serial code. Optionally also plots convergence for parallel method as well as speedup between methods.

Figures:
contours.png || Contours of solution
error_contours.png || Absolute error contours
convergence.png || Rate of convergence illustration
speedup.png || Speedup of parallel method
