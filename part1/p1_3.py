# Juha Leppanen, 00820470

"""
Test gradient routines in fdmodule2d
To build the .so module:
'make f2py'
OR
rm *.mod
gfortran -c fdmodule.f90 fdmodule2d.f90
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p1_3 -llapack
"""
from p1_3 import fdmodule as f1
from p1_3 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt

def test_grad1(n1,n2,numthreads):
    """call fortran test routines, test_grad,test_grad_omp,teat_grad2d
    with n2 x n1 matrices, run f2d_test_grad_omp with numthreads threads
    return errors and computation times
    """

    
    # initialise arrays and set parameters
    error = np.zeros(2)
    error_omp = np.zeros(2)
    error_2d = np.zeros(2)
    time = np.zeros(10)
    time_omp = np.zeros(10)
    time_2d = np.zeros(10)
    f2d.n1 = n1
    f2d.n2 = n2
    f2d.numthreads = numthreads

    # main loop calling the serial and parallel test subroutines
    for i in range(10):
        error, time[i] = f2d.test_grad()
        error_omp,time_omp[i] = f2d.test_grad_omp()
        error_2d,time_2d[i] = f2d.test_grad2d()

    return error, error_omp, error_2d, time.mean(), time_omp.mean(), time_2d.mean()
    
    
def test_gradN(numthreads):
    """
Assesses speedup going from serial 1D code to 2D code, as well as serial 1D code to parallel 1D code. We notice that the speedup is better with the 2D method, as DGTSV is highly optimised to solve these kind of systems, whereas our own implementation is quite crude and relies heavily on OpenMP for efficient memory allocation etc.
INPUTS:
    - numthreads = # of threads used in the parallel version
OUTPUTS: Plot of speedups
    - speedup1 = speedup between serial/parallel versions
    - speedup2 = speedup between serial/2D versions

    """
    # initialise variables
    n1_vals = range(100,1300,200)
    n2_vals = n1_vals
    speedup1 = np.zeros((np.size(n1_vals),np.size(n2_vals)))
    speedup2 = np.zeros((np.size(n1_vals),np.size(n2_vals)))
   
    # main loop to compute the speedups for chosen values of n
    for i in range(np.size(n1_vals)):
        for j in range(np.size(n2_vals)):
            f2d.n1 = n1_vals[i]
            f2d.n2 = n2_vals[j]
            e,e_omp,e_2d,time,time_omp,time_2d = test_grad1(f2d.n1,f2d.n2,2)
            speedup1[i,j] = time/time_omp
            speedup2[i,j] = time/time_2d
    
    # plot the speedups in same axes
    plt.figure()
    plt.plot(n1_vals, np.mean(speedup1,1),'*b',label='Parallel 1D version')
    plt.plot(n1_vals, np.mean(speedup2,1),'*r',label='2D version')
    plt.title('test_gradN, Juha Leppanen\nAverage speedup over values of N2 as N1 varies')
    plt.legend(loc='best')
    plt.xlabel('N1')
    plt.ylabel('Speedup')

    return speedup1,speedup2

if __name__ == "__main__":
    
    e,ep,e2d,t,tp,t2d = test_grad1(400,200,2)
    s,s2d = test_gradN(2) 
    plt.show()
