program test

!!$  use fdmodule
!!$  implicit none
!!$  integer :: n1,n2,numthreads
!!$  real(kind=8) :: dx1,dx2

!!$  integer :: i1,j1
!!$  real(kind=8), allocatable, dimension(:) :: x1,x2,D,DL,DU !coordinates stored in arrays
!!$  real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
!!$  real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2,df1_2,df2_2,df_t, f_t,df_amp
!!$  real(kind=8) :: error(4),df_max
!!$  integer :: INFO,i
!!$
!!$  
!!$  n1=7 !1 row, all columns
!!$  n2=6 !1 column, all rows
!!$  numthreads = 2
!!$
!!$    dx1 = 1.d0/dble(n1-1)
!!$    dx2 = 1.d0/dble(n2-1)
!!$    
!!$    allocate(x1(n1),x2(n2),D(n2),DL(n2-1),DU(n2-1),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1), &
!!$         df1_2(n2,n1),df2_2(n2,n1),df_t(n1,n2),f_t(n1,n2),df_amp(n2,n1))
!!$
!!$    do i1=1,n1
!!$        x1(i1) = dble(i1-1)*dx1
!!$    end do
!!$
!!$    do i1=1,n2
!!$        x2(i1) = dble(i1-1)*dx2
!!$    end do
!!$
!!$    do i1=1,n2
!!$        x11(i1,:) = x1
!!$    end do
!!$
!!$    do i1=1,n1
!!$        x22(:,i1) = x2
!!$    end do
!!$
!!$    ftest = sin(x11)*cos(x22) !test function    !generate grid
!!$    call testgrad(ftest,df1,df2,df_amp,df_max)
!!$    f_t = transpose(ftest)
!!$    
!!$    n = size(ftest,2)
!!$    dx = dx1
!!$    do i=1,n2
!!$       call cfd4(ftest(i,:),df1(i,:))
!!$    end do
!!$
!!$    n = size(ftest,1)
!!$    dx = dx2
!!$    do i=1,n1
!!$        call cfd4(ftest(:,i),df2(:,i))
!!$    end do
!!$    
!!$    
!!$    n = size(f_t,1)
!!$    dx = dx1
!!$    call cfd4_2d(f_t,df_t)
!!$    df1_2 = transpose(df_t)
!!$
!!$    n = size(ftest,1)
!!$    dx = dx2
!!$    call cfd4_2d(ftest,df2_2)
!!$
!!$
!!$
!!$    error(1) = sum(abs(df1-cos(x11)*cos(x22)))/(n1*n2)
!!$    error(2) = sum(abs(df2+sin(x11)*sin(x22)))/(n1*n2)
!!$    error(3) = sum(abs(df1_2-cos(x11)*cos(x22)))/(n1*n2)
!!$    error(4) = sum(abs(df2_2+sin(x11)*sin(x22)))/(n1*n2)
!!$    print *,error
!!$    deallocate(x1,x2,x11,x22,D,DL,DU,ftest,df_t,f_t,df1,df2,df1_2,df2_2)


end program test
