! Juha Leppanen, 00820470
!Run with 2 cores with 'make mpi' then 'make testmpi'
!mpif90 -O3 -o adv_mpi.exe ode_mpi.f90
!to run mpiexec -n 2 adv_mpi.exe (using 2 processes in this example, could be any other number)
module advmodule
    implicit none
    real(kind=8) :: S_adv,c_adv
end module advmodule
!-------------------------------
program advection_mpi
    use mpi
    use advmodule
    implicit none
    integer :: i1,j1
    integer :: nt,n !number of time steps, number of spatial points
    real(kind=8) :: dt,dx,pi !time step, grid size, advection eqn parameters
    integer :: myid, numprocs, ierr
    real(kind=8), allocatable, dimension(:) :: x,f0,f !grid, initial condition, solution

 ! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

!gather input
    open(unit=10,file='data_mpi.in')
        read(10,*) n
        read(10,*) nt
        read(10,*) dt
        read(10,*) c_adv
        read(10,*) S_adv
    close(10)

    allocate(x(n),f0(n),f(n))

!make grid from 0 to 1-dx, dx = 1/n
    do i1=1,n
        x(i1) = dble(i1-1)/dble(n)
    end do
    dx = 1.d0/dble(n)

!generate initial condition
    pi = acos(-1.d0)
    f0 = sin(2.d0*pi*x)


!compute solution
    call euler_mpi(MPI_COMM_WORLD,numprocs,n,0.d0,f0,dt,nt,dx,f)


!output solution
        call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
       if (myid==0) then
        print *, 'max(f)=',maxval(abs(f))
        open(unit=11,file='fmpi.dat')
        do i1=1,n
            write(11,*) f(i1)
        end do
        close(11)
    end if
    !can be loaded in python with: f=np.loadtxt('fmpi.dat')

    call MPI_FINALIZE(ierr)
end program advection_mpi
!-------------------------------


subroutine euler_mpi(comm,numprocs,n,t0,y0,dt,nt,dx,y)
    !explicit Euler method
    use mpi
    use advmodule
    implicit none
    integer, intent (in) :: n,nt
    real(kind=8), dimension(n), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt,dx
    real(kind=8), dimension(n), intent(out) :: y
    real(kind=8), allocatable,dimension(:) :: ylocal,Rpart
    real(kind=8) :: t
    integer :: i,k,istart,iend,nlocal
    ! Variables needed for MPI calls
    integer, dimension(MPI_STATUS_SIZE) :: status
    integer :: comm,myid,ierr,numprocs,sender,receiver
    ! Variables needed for gathering data at the end
    integer, allocatable, dimension(:) :: nper_proc, seams 

    call MPI_COMM_RANK(comm, myid, ierr)
    print *, 'start euler_mpi, myid=',myid

    !set initial conditions
    y = y0
    t = t0

    !generate decomposition and allocate sub-domain variables
    call mpe_decomp1d(size(y),numprocs,myid,istart,iend)
    nlocal = iend-istart+1

    allocate(ylocal(nlocal+2),Rpart(nlocal))
    ylocal(2:nlocal+1) = y0(istart:iend)

    print *, 'istart,iend,threadID,npart=',istart,iend,myid
    
    ! Define senders/receivers
    if (myid<numprocs-1) then
        receiver = myid+1
    else
         receiver = 0
    end if

    if (myid>0) then
        sender = myid-1
    else
        sender = numprocs-1
    end if

    !time marching
    do k = 1,nt
       ! Send and receive the extra boundary points for 2nd order stencil
       call MPI_SEND(ylocal(nlocal+1),1,MPI_DOUBLE_PRECISION,receiver,0,comm,ierr)
       call MPI_RECV(ylocal(1),1,MPI_DOUBLE_PRECISION,sender,MPI_ANY_TAG,comm,status,ierr)
       call MPI_BARRIER(comm,ierr)
       call MPI_SEND(ylocal(2),1,MPI_DOUBLE_PRECISION,sender,(nlocal+1),comm,ierr)
       call MPI_RECV(ylocal(nlocal+2),1,MPI_DOUBLE_PRECISION,receiver,MPI_ANY_TAG,comm,status,ierr)
       call MPI_BARRIER(comm,ierr)

       ! Call the RHS function

        call RHS_mpi(size(ylocal),dx,t,ylocal,Rpart)

        ! ------Step in space------
        ! Update the interior points of ylocal, boundary points will be updated from neighbours in the next send/rcv phase
        ylocal(2:nlocal+1)= ylocal(2:nlocal+1) + dt*Rpart
     
        ! ------Step in time------
        t = t + dt

        call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    end do

    print *, 'before collection',myid, maxval(abs(ylocal))
    !collect ylocal from each processor onto myid=0

    ! Initialise the information needed for gathering the result to process 0, we have used code from HW5 as guideline for this part
    allocate(nper_proc(numprocs),seams(numprocs))
    call MPI_GATHER(nlocal,1,MPI_INT,nper_proc,1,MPI_INT,0,comm,ierr)
    
    if (myid==0) then
        print *,'Nper_proc=',nper_proc
        seams(1)=0
        do i=2,numprocs
            seams(i) = seams(i-1)+Nper_proc(i-1)
        end do
        print *, 'disps=', seams
    end if
    
    call MPI_GATHERV(ylocal,nlocal,MPI_DOUBLE_PRECISION,y,Nper_proc, &
                seams,MPI_DOUBLE_PRECISION,0,comm,ierr)
    if (myid==0) print *, 'finished',maxval(abs(y))



end subroutine euler_mpi
!-------------------------
subroutine RHS_mpi(nn,dx,t,f,rhs)
    use advmodule
    implicit none
    integer, intent(in) :: nn
    real(kind=8), intent(in) :: dx,t
    real(kind=8), dimension(nn), intent(in) :: f
    real(kind=8), dimension(nn-2), intent(out) :: rhs
    real(kind=8),dimension(nn-2) :: df

    df = (f(3:nn) - f(1:nn-2))/(2.d0*dx) ! Compute df using 2nd order stencil
    rhs = s_adv - c_adv*df ! Compute RHS using df
end subroutine RHS_mpi

!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D


