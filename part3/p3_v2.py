""" Juha Leppanen, 00820470
Project part 3.
Solve the 1D advection equation in Fortran.
Compile everything using 'make f2py'.
Also contains code to run MPI implementation with the same parameters for convenience, but have to add 'fmpi' to a few function calls/return statements.
"""
import numpy as np
import matplotlib.pyplot as plt
import adv
from subprocess import call

#f2py -llapack -c fdmoduleB.f90 ode.f90 advmodule.f90 -m adv --f90flags='-fopenmp' -lgomp (or make f2py)

def advection1f(nt,tf,n,dx,c=1.0,S=0.0,display=False,numthreads=1,includeMPI=False):
    """Solve advection equation, df/dt + c df/dx = S
    for x=0,dx,...,(n-1)*dx, and returns f(x,tf),fp(x,tf),
    and f4(x,tf) which are solutions obtained using the fortran
    routines ode_euler, ode_euler_omp, ode_rk4
    -    f(x,t=0) = sin(2 pi x/L), L = n*dx
    -    nt time steps are taken from 0 to tf
    -    The solutions are plotted if display is true
    -    If includeMPI=True, also runs the MPI implementation with the same parameters
    """
    # Define parameters for modules
    x = np.arange(n)*dx # Grid in space
    f0 = np.sin(2*np.pi*x/(n*dx)) # Initial condition
    adv.advmodule.c_adv = c
    adv.advmodule.s_adv = S
    adv.fdmodule.dx = dx
    adv.fdmodule.n = n

    # Main function calls
    f = adv.ode.euler(0.0,f0,tf/nt,nt)
    fp = adv.ode.euler_omp(0.0,f0,tf/nt,nt,numthreads)
    f4 = adv.ode.rk4(0.0,f0,tf/nt,nt)

    # Following code writes parameter input file, compiles and runs MPI version if flag is True
    if (includeMPI == True):
        # Write parameters in the input file for the MPI program
        parameters = [n,nt,tf/float(nt),c,S]
        mpi_input = open('data_mpi.in','r+')
        mpi_input.truncate()
        for i in range(np.size(parameters)):
            mpi_input.write(str(parameters[i]))
            mpi_input.write('\n')
        mpi_input.close()
    
        # Call the MPI program in shell and read the output
        call(["mpif90", "-O3", "-o", "adv_mpi.exe", "ode_mpi.f90"])
        call(["mpiexec", "-n", str(numthreads), "adv_mpi.exe"])
        f_mpi = np.loadtxt('fmpi.dat')

        return f,fp,f4,fmpi # Need to add a 'fmpi' to receive fourth output if used
    
    return f, fp, f4

def test_advection1f(n):
    """
    Compute scaled L1 errors for solutions with n points 
    produced by advection1f when c=1,S=1,tf=1,nt=16000, L=1.

    """

    # Define parameters
    L = 1.0
    c = 1.0
    S = 1.0
    dx = L/float(n)
    tf = 1.0
    nt = 16000
    x = np.arange(n)*dx
    numthreads = 2

    # Obtain solutions from different methods by calling advection1f
    f, fp, f4 = advection1f(nt,tf,n,dx,c,S,False,numthreads)

    # Compute errors
    e = sum(abs(f-(np.sin(2*np.pi/L*(x-c*tf))+S*tf)))/n
    ep = sum(abs(fp-(np.sin(2*np.pi/L*(x-c*tf))+S*tf)))/n
    e4 = sum(abs(f4-(np.sin(2*np.pi/L*(x-c*tf))+S*tf)))/n
    
    return e,ep,e4 #errors from euler, euler_omp, rk4
        
"""This section is included for assessment and must be included as is in the final
file that you submit,"""
if __name__ == '__main__':
    n = 200
    nt = 320000
    tf = 1.21
    dx = 1.0/n
    f,fp,f4 = advection1f(nt,tf,n,dx,1,0)
    e,ep,e4 = test_advection1f(200)
    eb,epb,e4b = test_advection1f(400)
    print e,ep,e4
    print eb,epb,e4b
    print e/eb,ep/epb,e4/e4b
    plt.show()
